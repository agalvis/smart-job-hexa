/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.application.services;

import cl.com.smartjob.smartjobhexa.domain.model.User;
import cl.com.smartjob.smartjobhexa.domain.port.input.UserServicePort;
import cl.com.smartjob.smartjobhexa.domain.port.output.UserPersistencePort;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserServicePort {

    private final UserPersistencePort userPersistencePort;

    private final PasswordEncoder passwordEncoder;

    private final String regex;

    public UserServiceImpl(
            UserPersistencePort userPersistencePort,
            PasswordEncoder passwordEncoder,
            @Value("${password-regex}") String regex) {
        this.userPersistencePort = userPersistencePort;
        this.passwordEncoder = passwordEncoder;
        this.regex = regex;
    }

    @Override
    public User save(User user) {
        validatePassword(user.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userPersistencePort.save(user);
    }

    private void validatePassword(String password) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(
                    "Password must have at least 8 characters,"
                            + "one uppercase, one lowercase,"
                            + "one number and one special character");
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userPersistencePort.findByEmail(email);
    }

    @Override
    public List<User> findAll() {
        return userPersistencePort.findAll();
    }
}
