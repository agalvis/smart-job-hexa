/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.domain.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Phone {
    private Long id;
    private String number;
    private String cityCode;
    private String countryCode;
}
