/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.domain.port.input;

import cl.com.smartjob.smartjobhexa.domain.model.User;
import java.util.List;
import java.util.Optional;

public interface UserServicePort {
    User save(User userDomain);

    Optional<User> findByEmail(String email);

    List<User> findAll();
}
