/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.domain.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
public class User {
    private UUID id;
    private String name;
    private String email;
    private String password;
    private Boolean isActive;
    private String token;
    private List<Phone> phones;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime lastLogin;
}
