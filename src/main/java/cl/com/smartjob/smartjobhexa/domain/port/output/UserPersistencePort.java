/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.domain.port.output;

import cl.com.smartjob.smartjobhexa.domain.model.User;
import java.util.List;
import java.util.Optional;

public interface UserPersistencePort {
    User save(User userDomain);

    Optional<User> findByEmail(String email);

    List<User> findAll();
}
