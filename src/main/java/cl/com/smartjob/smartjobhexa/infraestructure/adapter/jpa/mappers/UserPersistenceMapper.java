/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.jpa.mappers;

import cl.com.smartjob.smartjobhexa.domain.model.User;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.jpa.entities.UserEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserPersistenceMapper {

    UserEntity toUserEntity(User user);

    User toUser(UserEntity userEntity);

    List<User> toUsers(List<UserEntity> userEntities);
}
