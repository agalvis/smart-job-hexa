/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.exeptions;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ErrorResponse {

    private int code;
    private String message;
    private List<String> details;
    private LocalDateTime timestamp;
}
