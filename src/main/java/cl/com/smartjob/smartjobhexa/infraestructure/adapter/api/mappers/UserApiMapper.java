/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.mappers;

import cl.com.smartjob.smartjobhexa.domain.model.User;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto.request.RegisterRequest;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto.response.UserResponse;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserApiMapper {
    User toUser(RegisterRequest registerRequest);

    UserResponse toRegisterResponse(User user);

    List<UserResponse> toUserResponse(List<User> users);
}
