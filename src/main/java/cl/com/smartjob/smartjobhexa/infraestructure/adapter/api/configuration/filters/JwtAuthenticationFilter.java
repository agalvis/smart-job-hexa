/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.configuration.filters;

import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.configuration.TokenJwtConfig;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.jpa.entities.UserEntity;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.jpa.repositories.H2UserRepository;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@AllArgsConstructor
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final H2UserRepository repository;

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        String username = null;
        String password = null;

        try {
            UserEntity user =
                    new ObjectMapper().readValue(request.getInputStream(), UserEntity.class);
            username = user.getEmail();
            password = user.getPassword();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);

        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain,
            Authentication authResult)
            throws IOException, ServletException {

        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User) authResult.getPrincipal();
        String username = user.getUsername();
        // Collection<? extends GrantedAuthority> roles = authResult.getAuthorities();
        List<String> roles =
                authResult.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();
        /*Claims claims =
        Jwts.claims()
                .add("roles", new ObjectMapper().writeValueAsString(roles))
                .add("username", username)
                .build();*/

        String token = getToken(username);

        updateUser(username, token);

        response.addHeader(
                TokenJwtConfig.HEADER_AUTHORIZATION, TokenJwtConfig.PREFIX_TOKEN + token);

        Map<String, String> body = new HashMap<>();
        body.put("token", token);
        body.put("username", username);

        response.getWriter().write(new ObjectMapper().writeValueAsString(body));
        response.setContentType(TokenJwtConfig.CONTENT_TYPE);
        response.setStatus(200);
    }

    private void updateUser(String username, String token) {
        UserEntity found =
                repository
                        .findByEmail(username)
                        .orElseThrow(() -> new RuntimeException("User not found"));
        found.setToken(token);
        found.setLastLogin(LocalDateTime.now());
        repository.save(found);
    }

    public static String getToken(String username) {
        return Jwts.builder()
                .subject(username)
                // .claims(claims)
                .expiration(new Date(System.currentTimeMillis() + (5 * 60000)))
                .issuedAt(new Date())
                .signWith(TokenJwtConfig.SECRET_KEY)
                .compact();
    }

    @Override
    protected void unsuccessfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException failed)
            throws IOException, ServletException {
        Map<String, String> body = new HashMap<>();
        body.put("errorDescription", "Authentication failed");
        body.put("errorCode", HttpStatus.UNAUTHORIZED.value() + "");

        response.getWriter().write(new ObjectMapper().writeValueAsString(body));
        response.setStatus(401);
        response.setContentType(TokenJwtConfig.CONTENT_TYPE);
    }
}
