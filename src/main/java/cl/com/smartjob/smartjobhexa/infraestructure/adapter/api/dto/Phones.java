/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto;

public record Phones(String number, String cityCode, String countryCode) {}
