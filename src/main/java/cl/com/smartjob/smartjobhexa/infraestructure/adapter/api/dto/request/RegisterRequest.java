/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto.request;

import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto.Phones;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.util.List;

public record RegisterRequest(
        @NotBlank(message = "Name required") String name,
        @NotBlank(message = "Email required")
                @Pattern(
                        regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$",
                        message = "Email not valid")
                String email,
        @NotBlank(message = "Password required") String password,
        @NotNull(message = "Phones required") List<Phones> phones) {

    @Override
    public String toString() {
        return "RegisterRequest{"
                + "name='"
                + name
                + '\''
                + ", email='"
                + email
                + '\''
                + ", password='"
                + password
                + '\''
                + ", phones="
                + phones
                + '}';
    }
}
