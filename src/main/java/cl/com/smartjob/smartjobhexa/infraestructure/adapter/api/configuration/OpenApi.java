/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.configuration;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SecurityScheme(
        name = "Bearer Authentication",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer")
public class OpenApi {

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList("Bearer Authentication {token}"))
                .info(
                        new Info()
                                .title("SmartJob APIS")
                                .description("Technical test api")
                                .version("1.0")
                                .contact(
                                        new Contact()
                                                .name("Alexander Galvis R")
                                                .email("galcorp@gmail.com")));
    }
}
