/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto.response;

import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto.Phones;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public record UserResponse(
        UUID id,
        String name,
        String email,
        Boolean isActive,
        // String token,
        List<Phones> phones,
        LocalDateTime createdAt,
        LocalDateTime updatedAt,
        LocalDateTime lastLogin) {}
