/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.controller;

import cl.com.smartjob.smartjobhexa.domain.port.input.UserServicePort;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto.request.RegisterRequest;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.dto.response.UserResponse;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.mappers.UserApiMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private final UserServicePort service;
    private final UserApiMapper mapper;

    @GetMapping
    @Operation(
            summary = "Get all users",
            description = "Get all users from database",
            parameters = {
                @Parameter(in = ParameterIn.HEADER, name = "Authorization", required = true)
            })
    @SecurityRequirement(name = "Bearer Authentication")
    public List<UserResponse> list() {
        return mapper.toUserResponse(service.findAll());
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(mapper.toRegisterResponse(service.save(mapper.toUser(request))));
    }
}
