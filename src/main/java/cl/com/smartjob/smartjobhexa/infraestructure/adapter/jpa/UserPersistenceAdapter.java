/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.jpa;

import cl.com.smartjob.smartjobhexa.domain.model.User;
import cl.com.smartjob.smartjobhexa.domain.port.output.UserPersistencePort;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.jpa.mappers.UserPersistenceMapper;
import cl.com.smartjob.smartjobhexa.infraestructure.adapter.jpa.repositories.H2UserRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserPersistenceAdapter implements UserPersistencePort {

    private final H2UserRepository userRepository;
    private final UserPersistenceMapper mapper;

    @Override
    public User save(User user) {
        return mapper.toUser(userRepository.save(mapper.toUserEntity(user)));
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email).map(mapper::toUser);
    }

    @Override
    public List<User> findAll() {
        return mapper.toUsers(userRepository.findAll());
    }
}
