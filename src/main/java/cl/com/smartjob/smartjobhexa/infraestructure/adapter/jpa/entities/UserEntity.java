/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.infraestructure.adapter.jpa.entities;

import cl.com.smartjob.smartjobhexa.infraestructure.adapter.api.configuration.filters.JwtAuthenticationFilter;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "users")
@JsonPropertyOrder({
    "id",
    "name",
    "email",
    "password",
    "isActive",
    "token",
    "phones",
    "createdAt",
    "updatedAt",
    "lastLogin"
})
@Getter
@Setter
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "uuid2")
    private UUID id;

    private String name;

    @Column(unique = true)
    @Pattern(
            regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$",
            message = "Formato de correo electrónico inválido")
    @JsonProperty(value = "email")
    @JsonAlias("username")
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private Boolean isActive;
    private String token;

    @OneToMany(cascade = CascadeType.ALL)
    private List<PhoneEntity> phones;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime lastLogin;

    @PrePersist
    public void prePersist() {
        createdAt = LocalDateTime.now();
        isActive = Boolean.TRUE;
        token = JwtAuthenticationFilter.getToken(email);
    }

    @PreUpdate
    public void preUpdate() {
        updatedAt = LocalDateTime.now();
    }
}
