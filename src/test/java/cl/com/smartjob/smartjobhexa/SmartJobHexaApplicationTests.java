/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SmartJobHexaApplicationTests {

    @Test
    void contextLoads() {}
}
