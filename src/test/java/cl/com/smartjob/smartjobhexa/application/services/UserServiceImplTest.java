/* (C) 2024 agalvis */
package cl.com.smartjob.smartjobhexa.application.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import cl.com.smartjob.smartjobhexa.domain.model.User;
import cl.com.smartjob.smartjobhexa.domain.port.output.UserPersistencePort;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

/** Unit tests for {@link UserServiceImpl}. */
@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock private UserPersistencePort userPersistencePort;

    @Mock private PasswordEncoder passwordEncoder;

    @InjectMocks private UserServiceImpl userService;

    private static final EasyRandom generator = new EasyRandom();
    private static final User user = generator.nextObject(User.class);
    private static final String regex =
            "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[@#$%^&*!])[A-Za-z\\d@#$%^&*!]{8,}$";

    @BeforeEach
    public void setUp() {
        userService = new UserServiceImpl(userPersistencePort, passwordEncoder, regex);
    }

    /** Method under test: {@link UserServiceImpl#save(User)} */
    @Test
    void save() {
        // Given
        user.setPassword("TestPassword123!");

        when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");
        when(userPersistencePort.save(any(User.class))).thenReturn(user);

        // When
        User savedUser = userService.save(user);

        // Then
        assertNotNull(savedUser);
        assertEquals("encodedPassword", savedUser.getPassword());
        verify(userPersistencePort, times(1)).save(user);
    }

    /** Method under test: {@link UserServiceImpl#save(User)} */
    @Test
    void saveErrorPassword() {
        // Given
        // When
        // Act and Assert
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> userService.save(user));
        assertEquals(
                "Password must have at least 8 characters,"
                        + "one uppercase, "
                        + "one lowercase,one number and one special character",
                exception.getMessage());
    }

    /** Method under test: {@link UserServiceImpl#findByEmail(String)} */
    @Test
    void findByEmail() {
        // Given
        Optional<User> userOptional = Optional.of(user);
        // When
        when(userPersistencePort.findByEmail(anyString())).thenReturn(userOptional);
        // Act and Assert
        Optional<User> findByEmail = userService.findByEmail(user.getEmail());
        assertNotEquals(Optional.empty(), findByEmail);
        assertEquals(userOptional, findByEmail);
    }

    /** Method under test: {@link UserServiceImpl#findAll()} */
    @Test
    void testFindAll() {
        // Arrange
        ArrayList<User> userList = new ArrayList<>();
        when(userPersistencePort.findAll()).thenReturn(userList);

        // Act
        List<User> actualFindAllResult = userService.findAll();

        // Assert
        verify(userPersistencePort).findAll();
        assertTrue(actualFindAllResult.isEmpty());
        assertSame(userList, actualFindAllResult);
    }
}
