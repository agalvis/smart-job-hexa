# Smart-job

# Getting Started cloud

* https://smart-job-wkd2.onrender.com/

# Getting Started Local
* Required Docker

```bash
docker build -t smart-job:1.0 .

docker run -d -p 8080:8080 --name smart-job smart-job:1.0
```
### Swagger

* [swagger-local](http://localhost:8080/smart-job/swagger-ui/index.html)
* [swagger-cloud](https://smart-job-wkd2.onrender.com/smart-job/swagger-ui/index.html)

### Postman

* [download](https://drive.google.com/file/d/1j2ewMM21NkCMXT0R35sXdR3EOuUem4xq/view?usp=drive_link)

## Endpoints de la API

| Method | Endpoint                   | Description                  |
|--------|----------------------------|------------------------------|
| POST   | /smart-job/users/register  | Register user                |
| POST   | /smart-job/login           | Login                        |
| GET    | /smart-job/users           | Get all users required token |
| GET    | /smart-job/actuator/health | Health check                 |

* /smart-job/users/register
```bash
curl --location 'localhost:8080/smart-job/users/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Juan Rodriguez",
    "email": "juan@rodriguez.org",
    "password": "Password123!",
    "phones": [
        {
            "number": "1234567",
            "cityCode": "1",
            "countryCode": "57"
        }
    ]
}'
```

* /smart-job/login
```bash
curl --location 'localhost:8080/smart-job/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "juan@rodriguez.org",
    "password": "Password123!"
}'
```

* /smart-job/users
```bash
curl --location 'localhost:8080/smart-job/users' \
--header 'Authorization: Bearer {{token}}'
```

*/smart-job/actuator/health
```bash
curl --location 'localhost:8080/smart-job/actuator/health'
```

## Diagrams
* Diagram of sequence - Register
![Diagram of sequence - Register](./src/main/resources/static/Register.png)
* Diagram of sequence - GetUsers
  ![Diagram of sequence - GetUsers](./src/main/resources/static/Get.png)
* Diagram of sequence - Database model
  ![Diagram of sequence - GetUsers](./src/main/resources/static/MER.png)